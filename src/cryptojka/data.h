//Here include all libreries
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <locale.h>
#include <libintl.h>

//Here all constant are defined

#define MAX_TEXT 1048576
#define MAX_PASS 64
#define VERSION "0.5.1"
